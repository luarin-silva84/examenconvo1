﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinFuegos
{
    public partial class FrmExtinguidoresCliente : Form
    {
        private DataSet dsExtinguidores;
        private BindingSource bsExtinguidores;

        public FrmExtinguidoresCliente()
        {
            InitializeComponent();
            bsExtinguidores = new BindingSource();
        }

        public DataSet DsExtinguidores
        {
            set
            {
                dsExtinguidores = value;
            }
        }

        private void FrmExtinguidoresCliente_Load(object sender, EventArgs e)
        {
            cmbFilter.DisplayMember = "Nombres";
            cmbFilter.ValueMember = "Id";
            cmbFilter.DataSource = dsExtinguidores.Tables["Cliente"];

            bsExtinguidores.DataMember = dsExtinguidores.Tables["Extinguidor"].TableName;
            bsExtinguidores.DataSource = dsExtinguidores;

            dgvExtinguidores.DataSource = bsExtinguidores;
            dgvExtinguidores.AutoGenerateColumns = true;
        }

        private void cmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            bsExtinguidores.Filter = string.Format("Cliente = {0}", cmbFilter.SelectedValue);
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            FrmReporte fr = new FrmReporte();
            fr.DsSistema = dsExtinguidores;
            fr.Show();
        }
    }
}
