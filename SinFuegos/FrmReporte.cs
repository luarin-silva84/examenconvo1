﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinFuegos
{
    public partial class FrmReporte : Form
    {
        private DataSet dsSistema;

        public FrmReporte()
        {
            InitializeComponent();
        }

        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }

        private void FrmReporte_Load(object sender, EventArgs e)
        {
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "SinFuegos.ReporteExtinguidores.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsExtinguidores", dsSistema.Tables["Extinguidor"]);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);
            this.reportViewer1.RefreshReport();
        }
    }
}
