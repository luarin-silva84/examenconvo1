﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinFuegos
{
    public partial class FrmGestionExtinguidores : Form
    {
        private DataSet dsExtinguidores;
        private BindingSource bsEctinguidores;

        public DataSet DsExtinguidores
        {
            set
            {
                dsExtinguidores = value;
            }
        }

        public FrmGestionExtinguidores()
        {
            InitializeComponent();
            bsEctinguidores = new BindingSource();
        }

        private void FrmGestionExtinguidores_Load(object sender, EventArgs e)
        {
            bsEctinguidores.DataSource = dsExtinguidores;
            bsEctinguidores.DataMember = dsExtinguidores.Tables["Extinguidor"].TableName;

            dgvExtinguidores.DataSource = bsEctinguidores;
            dgvExtinguidores.AutoGenerateColumns = true;
        }

        private void txtFilter_TextChanged(object sender, EventArgs e)
        {
            bsEctinguidores.Filter = string.Format("Categoria like '%{0}%' or TipoExtinguidor like '%{0}%'", txtFilter.Text);
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            FrmExtinguidor fc = new FrmExtinguidor();
            fc.DsSistema = dsExtinguidores;
            fc.TblExtinguidores = dsExtinguidores.Tables["Extinguidor"];
            fc.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvExtinguidores.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            FrmExtinguidor fc = new FrmExtinguidor();
            fc.DsSistema = dsExtinguidores;
            fc.TblExtinguidores = dsExtinguidores.Tables["Extinguidor"];
            fc.DrExtinguidor = drow;
            fc.ShowDialog();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvExtinguidores.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "Debe seleccionar una fila de la tabla para poder editar", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataGridViewRow gridRow = rowCollection[0];
            DataRow drow = ((DataRowView)gridRow.DataBoundItem).Row;

            DialogResult result = MessageBox.Show(this, "Realmente desea eliminar este registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                dsExtinguidores.Tables["Extinguidor"].Rows.Remove(drow);
                MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void dgvExtinguidores_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
