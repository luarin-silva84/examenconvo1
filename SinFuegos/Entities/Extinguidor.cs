﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinFuegos.Entities
{
    class Extinguidor
    {
        private int id;
        private Categoria categoria;
        private string tipoExtinguidor;
        private Marca marca;
        private Capacidad capacidad;
        private UnidadMedida unidadMedida;
        private string lugarDesignado;
        private DateTime fechaRecarga;
        private Cliente cliente;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        internal Categoria Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        public string TipoExtinguidor
        {
            get
            {
                return tipoExtinguidor;
            }

            set
            {
                tipoExtinguidor = value;
            }
        }

        internal Marca Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        internal Capacidad Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        internal UnidadMedida UnidadMedida
        {
            get
            {
                return unidadMedida;
            }

            set
            {
                unidadMedida = value;
            }
        }

        public string LugarDesignado
        {
            get
            {
                return lugarDesignado;
            }

            set
            {
                lugarDesignado = value;
            }
        }

        public DateTime FechaRecarga
        {
            get
            {
                return fechaRecarga;
            }

            set
            {
                fechaRecarga = value;
            }
        }

        internal Cliente Cliente
        {
            get
            {
                return cliente;
            }

            set
            {
                cliente = value;
            }
        }

        public Extinguidor(int id, Categoria categoria, string tipoExtinguidor, Marca marca, Capacidad capacidad, UnidadMedida unidadMedida, string lugarDesignado, DateTime fechaRecarga, Cliente cliente)
        {
            this.Id = id;
            this.Categoria = categoria;
            this.TipoExtinguidor = tipoExtinguidor;
            this.Marca = marca;
            this.Capacidad = capacidad;
            this.UnidadMedida = unidadMedida;
            this.LugarDesignado = lugarDesignado;
            this.FechaRecarga = fechaRecarga;
            this.Cliente = cliente;
        }
    }

    enum Categoria
    {
        Americano,
        Europeo
    }

    enum Marca
    {
        Tornado,
        Amerex,
        Otros
    }

    enum Capacidad
    {
        Cinco,
        Diez,
        Veinte,
        Cincuenta
    }

    enum UnidadMedida
    {
        Litros,
        Libras
    }
}
