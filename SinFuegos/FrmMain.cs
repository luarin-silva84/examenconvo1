﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinFuegos
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionClientes fgc = new FrmGestionClientes();
            fgc.MdiParent = this;
            fgc.DsClientes = dsSistema;
            fgc.Show();
        }

        private void extintoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionExtinguidores fge = new FrmGestionExtinguidores();
            fge.MdiParent = this;
            fge.DsExtinguidores = dsSistema;
            fge.Show();
        }

        private void extinguidoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmExtinguidoresCliente fec = new FrmExtinguidoresCliente();
            fec.MdiParent = this;
            fec.DsExtinguidores = dsSistema;
            fec.Show();
        }
    }
}
