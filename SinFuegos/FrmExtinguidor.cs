﻿using SinFuegos.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinFuegos
{
    public partial class FrmExtinguidor : Form
    {
        private DataSet dsSistema;
        private DataTable tblExtinguidores;
        private DataRow drExtinguidor;

        public DataTable TblExtinguidores
        {
            set
            {
                tblExtinguidores = value;
            }
        }

        public DataRow DrExtinguidor
        {
            set
            {
                drExtinguidor = value;
            }
        }

        public DataSet DsSistema
        {
            set
            {
                dsSistema = value;
            }
        }

        public FrmExtinguidor()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string categoria, tipo, marca, capacidad, unidad, destino, recarga;
            int cliente = (int) cmbCliente.SelectedValue;
            categoria = cmbCategoria.SelectedItem.ToString();
            tipo = txtTipo.Text;
            marca = cmbMarca.SelectedItem.ToString();
            capacidad = cmbCapacidad.SelectedItem.ToString();
            unidad = cmbUnidadMedida.SelectedItem.ToString();
            destino = txtDestino.Text;
            recarga = dtpRecarga.Text;

            if (drExtinguidor != null)
            {
                DataRow drNew = tblExtinguidores.NewRow();

                int index = tblExtinguidores.Rows.IndexOf(drExtinguidor);
                drNew["Id"] = drExtinguidor["Id"];
                drNew["Categoria"] = categoria;
                drNew["TipoExtinguidor"] = tipo;
                drNew["Marca"] = marca;
                drNew["Capacidad"] = capacidad;
                drNew["UnidadMedida"] = unidad;
                drNew["LugarDestino"] = destino;
                drNew["FechaRecarga"] = recarga;
                drNew["Cliente"] = cliente;


                tblExtinguidores.Rows.RemoveAt(index);
                tblExtinguidores.Rows.InsertAt(drNew, index);
                tblExtinguidores.Rows[index].AcceptChanges();
                tblExtinguidores.Rows[index].SetModified();
            }
            else
            {
                tblExtinguidores.Rows.Add(tblExtinguidores.Rows.Count + 1, tipo, categoria, marca, capacidad, unidad, destino, recarga, cliente);
            }

            Dispose();
        }

        private void FrmExtinguidor_Load(object sender, EventArgs e)
        {
            cmbCapacidad.DataSource = Enum.GetValues(typeof(Capacidad));
            cmbCategoria.DataSource = Enum.GetValues(typeof(Categoria));
            cmbMarca.DataSource = Enum.GetValues(typeof(Marca));
            cmbUnidadMedida.DataSource = Enum.GetValues(typeof(UnidadMedida));

            cmbCliente.DataSource = dsSistema.Tables["Cliente"];
            cmbCliente.DisplayMember = "Nombres";
            cmbCliente.ValueMember = "Id";
        }
    }
}
