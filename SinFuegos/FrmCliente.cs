﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SinFuegos
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataRow drCliente;

        public DataTable TblClientes
        {
            set
            {
                tblClientes = value;
            }
        }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
            }
        }

        public FrmCliente()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string nombre, cedula, apellidos, celular, correo, direccion, municipio, departamento;
            nombre = txtNombres.Text;
            cedula = txtCedula.Text;
            apellidos = txtApellidos.Text;
            celular = txtCelular.Text;
            correo = txtCorreo.Text;
            direccion = txtDireccion.Text;
            municipio = txtMunicipio.Text;
            departamento = txtDepartamento.Text;

            if (drCliente != null)
            {
                DataRow drNew = tblClientes.NewRow();

                int index = tblClientes.Rows.IndexOf(drCliente);
                drNew["Id"] = drCliente["Id"];
                drNew["Nombres"] = nombre;
                drNew["Cedula"] = cedula;
                drNew["Apellidos"] = apellidos;
                drNew["Celular"] = celular;
                drNew["Correo"] = correo;
                drNew["Direccion"] = direccion;
                drNew["Municipio"] = municipio;
                drNew["Departamento"] = departamento;


                tblClientes.Rows.RemoveAt(index);
                tblClientes.Rows.InsertAt(drNew, index);
                tblClientes.Rows[index].AcceptChanges();
                tblClientes.Rows[index].SetModified();
            }
            else
            {
                tblClientes.Rows.Add(tblClientes.Rows.Count + 1, cedula, nombre, apellidos, celular, correo, direccion, municipio, departamento);
            }

            Dispose();
        }
    }
}
